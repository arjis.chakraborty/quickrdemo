package com.internship.quickrdemo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.adapters.CommentsRecyclerAdapter;
import com.internship.quickrdemo.constants.Constants;
import com.internship.quickrdemo.models.CommentsModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommentsBottomSheetDialogFragment extends BottomSheetDialogFragment {
    View view;
    RecyclerView commentsRecycler;
    EditText comment;
    ImageView sendBtn;
    CommentsRecyclerAdapter adapter;
    
    FirebaseFirestore fStore;
    
    ArrayList<CommentsModel> arrayList = new ArrayList<>();
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_comment, container, false);
        
        commentsRecycler = view.findViewById(R.id.commentsRecycler);
        comment = view.findViewById(R.id.comment);
        sendBtn = view.findViewById(R.id.sendBtn);
        
        fStore = FirebaseFirestore.getInstance();
        
        adapter = new CommentsRecyclerAdapter();
        commentsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        commentsRecycler.setAdapter(adapter);
        
        fStore.collection("comments")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (document.getString("productId").equalsIgnoreCase(getArguments().getString("productId"))) {
                                    CommentsModel model = new CommentsModel();
                                    model.setId(document.getId());
                                    model.setComment(document.getString("comment"));
                                    model.setName(document.getString("name"));
                                    adapter.setItems(model);
                                    arrayList.add(model);
                                }
                            }
                            if(arrayList.size() == 0){
                                view.findViewById(R.id.noDataLayout).setVisibility(View.VISIBLE);
                            }
                        }
                        else{
                            view.findViewById(R.id.noDataLayout).setVisibility(View.VISIBLE);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
        
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!comment.getText().toString().isEmpty())
                    sendComment();
                else
                    Toast.makeText(getActivity(), "Please enter a comment", Toast.LENGTH_SHORT).show();
            }
        });
        
        return view;
    }
    
    void sendComment() {
        String commentId = System.currentTimeMillis() + "";

        String commentDes = this.comment.getText().toString();
        
        this.comment.setText("");
        
        Map<String, Object> comment = new HashMap<>();
        comment.put("productId", getArguments().getString("productId"));
        comment.put("name", Constants.SHARED_PREFERENCES.getString(Constants.USER_NAME, ""));
        comment.put("comment", commentDes);
        
        fStore.collection("comments")
                .document(commentId)
                .set(comment)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            CommentsModel model = new CommentsModel();
                            model.setId(commentId);
                            model.setName(Constants.SHARED_PREFERENCES.getString(Constants.USER_NAME, ""));
                            model.setComment(commentDes);
                            adapter.setItems(model);
                            view.findViewById(R.id.noDataLayout).setVisibility(View.GONE);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }
}

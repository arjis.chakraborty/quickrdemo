package com.internship.quickrdemo.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.constants.Constants;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class FillAdDetailsFragment extends Fragment {
    View view;
    String categoryName, categoryId;
    TextView name;
    RelativeLayout addPhotoLayout;
    TextInputLayout title, location, price, description;
    CheckBox spotlightCheck;
    ImageView pickedImage;
    Button postAdBtn;
    
    FirebaseFirestore fStore;
    FirebaseStorage fStorage;
    StorageReference reference;
    
    private final int PICK_IMAGE_REQUEST = 22;
    private final int STORAGE_PERMISSION_CODE = 10012;
    private Uri filePath;
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ad_details, container, false);
        
        fStore = FirebaseFirestore.getInstance();
        fStorage = FirebaseStorage.getInstance();
        reference = fStorage.getReference();
        
        categoryName = getArguments().getString("categoryName");
        categoryId = getArguments().getString("categoryId");
        
        name = view.findViewById(R.id.name);
        addPhotoLayout = view.findViewById(R.id.addPhotoLayout);
        title = view.findViewById(R.id.title);
        location = view.findViewById(R.id.location);
        price = view.findViewById(R.id.price);
        spotlightCheck = view.findViewById(R.id.spotlightCheck);
        pickedImage = view.findViewById(R.id.pickedImage);
        postAdBtn = view.findViewById(R.id.postAdBtn);
        description = view.findViewById(R.id.description);
        
        name.setText(categoryName);
        
        addPhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission();
            }
        });
        
        postAdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.getEditText().getText().toString().isEmpty()
                        || title.getEditText().getText().toString().isEmpty()
                        || location.getEditText().getText().toString().isEmpty()
                        || price.getEditText().getText().toString().isEmpty()
                        || description.getEditText().getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please fill all the fields", Toast.LENGTH_SHORT).show();
                } else {
                    uploadAllData();
                }
            }
        });
        
        return view;
    }
    
    public void checkPermission() {
        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } else {
            selectImage();
        }
    }
    
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                Toast.makeText(getActivity(), "Storage Permission Denied", Toast.LENGTH_SHORT).show();
            }
        } else {
            selectImage();
        }
    }
    
    private void selectImage() {
        // Defining Implicit Intent to mobile gallery
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image from here..."), PICK_IMAGE_REQUEST);
    }
    
    // Override onActivityResult method
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            // Get the Uri of data
            filePath = data.getData();
            try {
                // Setting image on image view using Bitmap
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                pickedImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                // Log the exception
                e.printStackTrace();
            }
        }
    }
    
    private void uploadAllData() {
        if (filePath != null) {
            String productId = System.currentTimeMillis() + "";
            
            // Code for showing progressDialog while uploading
            ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            
            // Defining the child of storageReference
            StorageReference ref = reference.child("products/" + productId);
            
            // adding listeners on upload
            // or failure of image
            ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // Image uploaded successfully
                    // Dismiss dialog
                    Map<String, Object> product = new HashMap<>();
                    product.put("categoryId", categoryId);
                    product.put("title", title.getEditText().getText().toString());
                    product.put("location", location.getEditText().getText().toString());
                    product.put("price", price.getEditText().getText().toString());
                    product.put("isSpotlight", spotlightCheck.isChecked());
                    product.put("description", description.getEditText().getText().toString());
                    product.put("userId", Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, ""));
                    
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            product.put("picURL", uri.toString());
                            progressDialog.dismiss();
                            fStore.collection("products")
                                    .document(productId)
                                    .set(product)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(getActivity(), "Ad successfully uploaded", Toast.LENGTH_SHORT).show();
                                            getActivity().finish();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                    
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Error, Image not uploaded
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new com.google.firebase.storage.OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                    double progress = (100.0 * snapshot.getBytesTransferred() / snapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploading " + (int) progress + "%");
                }
            });
        }
    }
}

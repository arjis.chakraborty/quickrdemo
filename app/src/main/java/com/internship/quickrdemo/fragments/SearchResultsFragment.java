package com.internship.quickrdemo.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.activities.SearchActivity;
import com.internship.quickrdemo.adapters.SearchResultsRecyclerAdapter;
import com.internship.quickrdemo.interfaces.OnSearchClickListener;
import com.internship.quickrdemo.models.SearchModel;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SearchResultsFragment extends Fragment {
    
    View view;
    RecyclerView searchResultsRecycler;
    SearchResultsRecyclerAdapter adapter;
    
    ArrayList<SearchModel> searchModelList = new ArrayList<>();
    String searchString;
    
    FirebaseFirestore fStore;
    RelativeLayout loadingLayout;
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search_results, container, false);
        
        searchResultsRecycler = view.findViewById(R.id.searchResultsRecycler);
        loadingLayout = view.findViewById(R.id.loadingLayout);
        
        fStore = FirebaseFirestore.getInstance();
        adapter = new SearchResultsRecyclerAdapter(new OnSearchClickListener() {
            @Override
            public void onClick(SearchModel searchModel) {
            
            }
        });
        searchResultsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        searchResultsRecycler.setAdapter(adapter);
        
        searchString = getArguments().getString("searchString");
        
        performSearch(searchString);
        
        return view;
    }
    
    private void performSearch(String searchString) {
        if (searchString.length() > 0) {
            fStore.collection("products")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    String categoryId = document.getString("categoryId");
                                    fStore.collection("categories")
                                            .document(categoryId)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        Log.e("DOCUMENT", task.getResult().getString("name") + ", " + searchString);
                                                        String categoryName = task.getResult().getString("name");
                                                        if (categoryName.toLowerCase().contains(searchString.toLowerCase())) {
                                                            Log.e("MATCHING", "true");
                                                            SearchModel model = new SearchModel();
                                                            model.setTitle(document.getString("title"));
                                                            model.setLocation(document.getString("location"));
                                                            model.setPicURL(document.getString("picURL"));
                                                            model.setId(document.getId());
                                                            model.setDescription(document.getString("description"));
                                                            DecimalFormat formatter = new DecimalFormat("#,###,###");
                                                            String formattedPrice = formatter.format(Long.parseLong(document.getString("price")));
                                                            model.setPrice(formattedPrice);
                                                            model.setUserId(document.getString("userId"));
                                                            searchModelList.add(model);
                                                            adapter.setItems(model);
                                                            view.findViewById(R.id.noDataLayout).setVisibility(View.GONE);
                                                        }
                                                        else{
                                                            view.findViewById(R.id.noDataLayout).setVisibility(View.VISIBLE);
                                                        }
                                                        loadingLayout.setVisibility(View.GONE);
                                                    }
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    e.printStackTrace();
                                                }
                                            });
                                }
                                
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}

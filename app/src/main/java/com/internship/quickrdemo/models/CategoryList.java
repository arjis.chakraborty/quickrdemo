package com.internship.quickrdemo.models;

public class CategoryList {
    String id, name, picURL;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getPicURL() {
        return picURL;
    }
    
    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }
}

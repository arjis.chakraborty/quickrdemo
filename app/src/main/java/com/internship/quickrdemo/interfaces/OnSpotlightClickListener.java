package com.internship.quickrdemo.interfaces;

import com.internship.quickrdemo.models.SpotlightModel;

public interface OnSpotlightClickListener {
    void onClick(SpotlightModel spotlightModel);
}

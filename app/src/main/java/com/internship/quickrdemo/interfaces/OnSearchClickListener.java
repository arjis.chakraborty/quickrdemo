package com.internship.quickrdemo.interfaces;

import com.internship.quickrdemo.models.SearchModel;

public interface OnSearchClickListener {
    void onClick(SearchModel searchModel);
}

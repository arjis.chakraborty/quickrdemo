package com.internship.quickrdemo.interfaces;

import com.internship.quickrdemo.models.CategoryList;

public interface OnCategoryClickListener {
    void onClick(CategoryList category);
}

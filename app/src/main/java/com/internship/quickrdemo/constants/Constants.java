package com.internship.quickrdemo.constants;

import android.content.SharedPreferences;

public class Constants {
    
    public static SharedPreferences SHARED_PREFERENCES;
    public static String SHARED_PREFERENCE_NAME = "quickr";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_PH = "user_ph";
    public static final String USER_PIC = "user_pic";
    public static final String USER_LOGGED = "user_logged";
    public static final String USER_EMAIL = "user_email";
}

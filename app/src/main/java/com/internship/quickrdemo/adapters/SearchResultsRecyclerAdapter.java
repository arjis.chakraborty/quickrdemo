package com.internship.quickrdemo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.internship.quickrdemo.R;
import com.internship.quickrdemo.interfaces.OnCategoryClickListener;
import com.internship.quickrdemo.interfaces.OnSearchClickListener;
import com.internship.quickrdemo.models.SearchModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SearchResultsRecyclerAdapter extends RecyclerView.Adapter<SearchResultsRecyclerAdapter.ViewHolder> {
    
    ArrayList<SearchModel> modelList = new ArrayList<>();
    OnSearchClickListener listener;
    
    public SearchResultsRecyclerAdapter(OnSearchClickListener listener) {
        this.listener = listener;
    }
    
    public void setItems(SearchModel model){
        modelList.add(model);
        notifyDataSetChanged();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_results, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return modelList.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(modelList.get(position).getPicURL())
                .resize(200, 200)
                .centerCrop()
                .placeholder(R.drawable.image_placeholder)
                .into(holder.icon);
    
        holder.title.setText(modelList.get(position).getTitle());
        holder.location.setText(modelList.get(position).getLocation());
        holder.price.setText("₹" + modelList.get(position).getPrice());
    }
    
    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView icon;
        TextView title, location, price;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);
            location = itemView.findViewById(R.id.location);
            price = itemView.findViewById(R.id.price);
            
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(modelList.get(getAdapterPosition()));
                }
            });
        }
    }
}

package com.internship.quickrdemo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.internship.quickrdemo.R;
import com.internship.quickrdemo.models.CommentsModel;

import java.util.ArrayList;

public class CommentsRecyclerAdapter extends RecyclerView.Adapter<CommentsRecyclerAdapter.ViewHolder>{
    
    ArrayList<CommentsModel> commentsList = new ArrayList();
    
    public void setItems(CommentsModel model){
        commentsList.add(model);
        notifyDataSetChanged();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comments, parent, false));
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(commentsList.get(position).getName() + " says : ");
        holder.comment.setText(commentsList.get(position).getComment());
    }
    
    @Override
    public int getItemCount() {
        return commentsList.size();
    }
    
    class ViewHolder extends RecyclerView.ViewHolder{
    
        TextView name, comment;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            
            name = itemView.findViewById(R.id.name);
            comment = itemView.findViewById(R.id.comment);
        }
    }
}

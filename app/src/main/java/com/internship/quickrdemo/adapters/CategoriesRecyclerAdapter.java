package com.internship.quickrdemo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.internship.quickrdemo.R;
import com.internship.quickrdemo.interfaces.OnCategoryClickListener;
import com.internship.quickrdemo.models.CategoryList;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.ViewHolder> {
    ArrayList<CategoryList> categoryLists = new ArrayList<>();
    
    OnCategoryClickListener listener;
    
    public CategoriesRecyclerAdapter(OnCategoryClickListener listener) {
        this.listener = listener;
    }
    
    public void setItems(CategoryList model) {
        categoryLists.add(model);
        notifyDataSetChanged();
    }
    
    public void setItems(CategoryList model, boolean shouldClear) {
        if (shouldClear)
            categoryLists.clear();
        categoryLists.add(model);
        notifyDataSetChanged();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return categoryLists.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso
                .get()
                .load(categoryLists.get(position).getPicURL())
                .placeholder(R.drawable.ic_image)
                .into(holder.icon);
        holder.categoryName.setText(categoryLists.get(position).getName());
    }
    
    public class ViewHolder extends RecyclerView.ViewHolder {
        
        ImageView icon;
        TextView categoryName;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            categoryName = itemView.findViewById(R.id.categoryName);
            
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(categoryLists.get(getAdapterPosition()));
                }
            });
        }
    }
}

package com.internship.quickrdemo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.internship.quickrdemo.R;
import com.internship.quickrdemo.interfaces.OnSpotlightClickListener;
import com.internship.quickrdemo.models.SpotlightModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NearbyAdsRecyclerAdapter extends RecyclerView.Adapter<NearbyAdsRecyclerAdapter.ViewHolder> {
    ArrayList<SpotlightModel> spotlightLists = new ArrayList<>();
    
    OnSpotlightClickListener listener;
    
    public NearbyAdsRecyclerAdapter(OnSpotlightClickListener listener) {
        this.listener = listener;
    }
    
    public void setItems(SpotlightModel model) {
        spotlightLists.add(model);
        notifyDataSetChanged();
    }
    
    public void setItems(SpotlightModel model, boolean shouldClear) {
        if (shouldClear)
            spotlightLists.clear();
        spotlightLists.add(model);
        notifyDataSetChanged();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nearby_ads, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return spotlightLists.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(spotlightLists.get(position).getPicURL())
                .resize(200, 200)
                .centerCrop()
                .placeholder(R.drawable.image_placeholder)
                .into(holder.icon);
        
        holder.title.setText(spotlightLists.get(position).getTitle());
        holder.location.setText(spotlightLists.get(position).getLocation());
        holder.price.setText("₹" + spotlightLists.get(position).getPrice());
    }
    
    public class ViewHolder extends RecyclerView.ViewHolder {
        
        ImageView icon;
        TextView title, location, price;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);
            location = itemView.findViewById(R.id.location);
            price = itemView.findViewById(R.id.price);
            
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(spotlightLists.get(getAdapterPosition()));
                }
            });
        }
    }
}

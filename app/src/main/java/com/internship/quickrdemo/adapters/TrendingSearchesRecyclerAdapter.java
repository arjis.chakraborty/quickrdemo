package com.internship.quickrdemo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.internship.quickrdemo.R;
import com.internship.quickrdemo.interfaces.OnCategoryClickListener;
import com.internship.quickrdemo.models.CategoryList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TrendingSearchesRecyclerAdapter extends RecyclerView.Adapter<TrendingSearchesRecyclerAdapter.ViewHolder> {
    ArrayList<CategoryList> categoryLists = new ArrayList<>();
    OnCategoryClickListener listener;
    
    public TrendingSearchesRecyclerAdapter(OnCategoryClickListener listener) {
        this.listener = listener;
    }
    
    public void setItems(CategoryList model) {
        categoryLists.add(model);
        notifyDataSetChanged();
    }
    
    public void setItems(CategoryList model, boolean shouldClear) {
        if (shouldClear)
            categoryLists.clear();
        categoryLists.add(model);
        notifyDataSetChanged();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trending_searches, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return categoryLists.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.categoryName.setText(categoryLists.get(position).getName());
    }
    
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.categoryName);
            
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(categoryLists.get(getAdapterPosition()));
                }
            });
        }
    }
}

package com.internship.quickrdemo.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.adapters.CategoriesRecyclerAdapter;
import com.internship.quickrdemo.adapters.NearbyAdsRecyclerAdapter;
import com.internship.quickrdemo.adapters.SpotlightRecyclerAdapter;
import com.internship.quickrdemo.interfaces.OnCategoryClickListener;
import com.internship.quickrdemo.interfaces.OnSpotlightClickListener;
import com.internship.quickrdemo.models.CategoryList;
import com.internship.quickrdemo.models.SpotlightModel;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    
    RecyclerView categoriesRecycler, spotlightRecycler, nearbyRecycler;
    SpotlightRecyclerAdapter spotlightRecyclerAdapter;
    CategoriesRecyclerAdapter categoriesRecyclerAdapter;
    NearbyAdsRecyclerAdapter nearbyAdsRecyclerAdapter;
    
    FirebaseFirestore fStore;
    
    ArrayList<CategoryList> categoryLists = new ArrayList<>();
    ArrayList<SpotlightModel> spotlightLists = new ArrayList<>();
    
    ExtendedFloatingActionButton fab;
    
    RelativeLayout searchLayout;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        categoriesRecycler = findViewById(R.id.categoriesRecycler);
        spotlightRecycler = findViewById(R.id.adsInSpotlightRecycler);
        nearbyRecycler = findViewById(R.id.nearAdsRecycler);
        searchLayout = findViewById(R.id.searchLayout);
        
        fab = findViewById(R.id.postAdBtn);
        
        fStore = FirebaseFirestore.getInstance();
    
        spotlightRecyclerAdapter = new SpotlightRecyclerAdapter(new OnSpotlightClickListener() {
            @Override
            public void onClick(SpotlightModel spotlightModel) {
                Intent intent = new Intent(MainActivity.this, ViewAdActivity.class);
                intent.putExtra("productId", spotlightModel.getId());
                intent.putExtra("title", spotlightModel.getTitle());
                intent.putExtra("description", spotlightModel.getDescription());
                intent.putExtra("location", spotlightModel.getLocation());
                intent.putExtra("price", spotlightModel.getPrice());
                intent.putExtra("picURL", spotlightModel.getPicURL());
                startActivity(intent);
            }
        });
        spotlightRecycler.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
        spotlightRecycler.setAdapter(spotlightRecyclerAdapter);
    
        categoriesRecyclerAdapter = new CategoriesRecyclerAdapter(new OnCategoryClickListener() {
            @Override
            public void onClick(CategoryList category) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra("shouldSearch", true);
                intent.putExtra("searchString", category.getName());
                startActivity(intent);
            }
        });
        categoriesRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        categoriesRecycler.setAdapter(categoriesRecyclerAdapter);
        
        nearbyAdsRecyclerAdapter = new NearbyAdsRecyclerAdapter(new OnSpotlightClickListener() {
            @Override
            public void onClick(SpotlightModel spotlightModel) {
                Intent intent = new Intent(MainActivity.this, ViewAdActivity.class);
                intent.putExtra("productId", spotlightModel.getId());
                intent.putExtra("title", spotlightModel.getTitle());
                intent.putExtra("description", spotlightModel.getDescription());
                intent.putExtra("location", spotlightModel.getLocation());
                intent.putExtra("price", spotlightModel.getPrice());
                intent.putExtra("picURL", spotlightModel.getPicURL());
                startActivity(intent);
            }
        });
        nearbyRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        nearbyRecycler.setAdapter(nearbyAdsRecyclerAdapter);
        
        fStore.collection("categories")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                CategoryList model = new CategoryList();
                                model.setId(document.getId());
                                model.setName(document.getString("name"));
                                model.setPicURL(document.getString("picURL"));
                                categoryLists.add(model);
                                categoriesRecyclerAdapter.setItems(model);
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.toString());
                e.printStackTrace();
            }
        });
        
        fStore.collection("products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                SpotlightModel model = new SpotlightModel();
                                boolean isSpotlight = document.getBoolean("isSpotlight");
                                model.setTitle(document.getString("title"));
                                model.setLocation(document.getString("location"));
                                model.setPicURL(document.getString("picURL"));
                                model.setDescription(document.getString("description"));
                                model.setId(document.getId());
                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                String formattedPrice = formatter.format(Long.parseLong(document.getString("price")));
                                model.setPrice(formattedPrice);
                                model.setUserId(document.getString("userId"));
                                if(isSpotlight){
                                    spotlightLists.add(model);
                                    spotlightRecyclerAdapter.setItems(model);
                                }
                                nearbyAdsRecyclerAdapter.setItems(model);
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.toString());
                e.printStackTrace();
            }
        });
        
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PostAdActivity.class);
                startActivity(intent);
            }
        });
        
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
    }
}
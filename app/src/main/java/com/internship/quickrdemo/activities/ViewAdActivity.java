package com.internship.quickrdemo.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.fragments.CommentsBottomSheetDialogFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ViewAdActivity extends AppCompatActivity {
    
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView image;
    
    TextView description, location, price;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_ad);
        
        toolbar = findViewById(R.id.toolbar);
        collapsingToolbarLayout = findViewById(R.id.collapsingToolbar);
        image = findViewById(R.id.image);
        description = findViewById(R.id.description);
        location = findViewById(R.id.location);
        price = findViewById(R.id.price);
        
        collapsingToolbarLayout.setTitle(getIntent().getStringExtra("title"));
        toolbar.setTitle(getIntent().getStringExtra("title"));
        
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        
        description.setText(getIntent().getStringExtra("description"));
        location.setText(getIntent().getStringExtra("location"));
        price.setText("₹" + getIntent().getStringExtra("price"));
        
        Picasso
                .get()
                .load(getIntent().getStringExtra("picURL"))
                .placeholder(R.drawable.image_placeholder)
                .into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        findViewById(R.id.loadingLayout).setVisibility(View.GONE);
                    }
                    
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                    }
                });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_message, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.comment:
                openCommentSection();
                return true;
    
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void openCommentSection(){
        CommentsBottomSheetDialogFragment bottomSheetFragment = new CommentsBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("productId", getIntent().getStringExtra("productId"));
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }
}

package com.internship.quickrdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.views.OtpEditText;

import java.util.concurrent.TimeUnit;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {
    
    private String verificationId;
    private FirebaseAuth mAuth;
    
    OtpEditText editOTP;
    Button continueBtn;
    TextView heading;
    
    String number;
    
    boolean isExisting = false;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        mAuth = FirebaseAuth.getInstance();
        editOTP = findViewById(R.id.editOTP);
        continueBtn = findViewById(R.id.continueBtn);
        heading = findViewById(R.id.heading);
        
        number = "+91" + getIntent().getStringExtra("phNum");
        
        isExisting = getIntent().getBooleanExtra("isExisting", false);
        
        heading.setText("We have sent a verification code to" + number + ". If the code is not automatically detected you can enter it manually in the field below.");
        continueBtn.setOnClickListener(this);
        
        sendVerificationCode(number);
    }
    
    private void sendVerificationCode(String number) {
        // this method is used for getting
        // OTP on user phone number.
        /*PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number, // first parameter is user's mobile number
                60, // second parameter is time limit for OTP
                // verification which is 60 seconds in our case.
                TimeUnit.SECONDS, // third parameter is for initializing units
                // for time period which is in seconds in our case.
                (Activity) TaskExecutors.MAIN_THREAD, // this task will be excuted on Main thread.
                mCallBack // we are calling callback method when we recieve OTP for
                // auto verification of user.
        );*/
    
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(number)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallBack)          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }
    
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            
            // initializing our callbacks for on
            // verification callback method.
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        
        // below method is used when
        // OTP is sent from Firebase
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            // when we receive the OTP it
            // contains a unique id which
            // we are storing in our string
            // which we have already created.
            verificationId = s;
        }
        
        // this method is called when user
        // receive OTP from Firebase.
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            // below line is used for getting OTP code
            // which is sent in phone auth credentials.
            final String code = phoneAuthCredential.getSmsCode();
            
            // checking if the code
            // is null or not.
            if (code != null) {
                // if the code is not null then
                // we are setting that code to
                // our OTP edittext field.
                editOTP.setText(code);
                
                // after setting this code
                // to OTP edittext field we
                // are calling our verifycode method.
                verifyCode(code);
            }
        }
        
        // this method is called when firebase doesn't
        // sends our OTP code due to any error or issue.
        @Override
        public void onVerificationFailed(FirebaseException e) {
            // displaying error message with firebase exception.
            Toast.makeText(VerificationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };
    
    private void verifyCode(String code) {
        // below line is used for getting
        // credentials from our verification id and code.
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        
        // after getting credential we are
        // calling sign in method.
        signInWithCredential(credential);
    }
    
    private void signInWithCredential(PhoneAuthCredential credential) {
        // inside this method we are checking if
        // the code entered is correct or not.
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if the code is correct and the task is successful
                            // we are sending our user to new activity.
                            Intent i;
                            if(isExisting){
                                i = new Intent(VerificationActivity.this, MainActivity.class);
                            }
                            else{
                                i = new Intent(VerificationActivity.this, AddDetailsActivity.class);
                                i.putExtra("phNum", number);
                            }
                            startActivity(i);
                            finish();
                        } else {
                            // if the code is not correct then we are
                            // displaying an error message to the user.
                            Toast.makeText(VerificationActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continueBtn:
                if (editOTP.getText() != null)
                    verifyCode(editOTP.getText().toString());
                else
                    Toast.makeText(this, "Please enter your verification code", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}

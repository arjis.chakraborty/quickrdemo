package com.internship.quickrdemo.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.adapters.TrendingSearchesRecyclerAdapter;
import com.internship.quickrdemo.fragments.SearchResultsFragment;
import com.internship.quickrdemo.interfaces.OnCategoryClickListener;
import com.internship.quickrdemo.models.CategoryList;
import com.internship.quickrdemo.models.SearchModel;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {
    
    RecyclerView trendingSearchesRecycler;
    EditText searchEdit;
    
    FirebaseFirestore fStore;
    
    TrendingSearchesRecyclerAdapter adapter;
    ArrayList<CategoryList> categoryLists = new ArrayList<>();
    
    Toolbar toolbar;
    RelativeLayout searchLayout;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        
        fStore = FirebaseFirestore.getInstance();
        
        trendingSearchesRecycler = findViewById(R.id.trendingSearchesRecycler);
        searchEdit = findViewById(R.id.searchEdit);
        toolbar = findViewById(R.id.toolbar);
        searchLayout = findViewById(R.id.searchLayout);
        
        searchEdit.requestFocus();
        
        setSupportActionBar(toolbar);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        
        adapter = new TrendingSearchesRecyclerAdapter(new OnCategoryClickListener() {
            @Override
            public void onClick(CategoryList category) {
                searchEdit.setText(category.getName());
                openSearchResults();
            }
        });
        
        trendingSearchesRecycler.setLayoutManager(new LinearLayoutManager(this));
        trendingSearchesRecycler.setAdapter(adapter);
        
        if(getIntent().getBooleanExtra("shouldSearch", false)){
            openSearchResults(getIntent().getStringExtra("searchString"));
        }
        
        fStore.collection("categories")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                CategoryList model = new CategoryList();
                                model.setId(document.getId());
                                model.setName(document.getString("name"));
                                model.setPicURL(document.getString("picURL"));
                                categoryLists.add(model);
                                adapter.setItems(model);
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.toString());
                e.printStackTrace();
            }
        });
        
        searchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    openSearchResults();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }
    
    void openSearchResults(String searchString) {
        if (searchString.length() > 0) {
            SearchResultsFragment fragment = new SearchResultsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("searchString", searchString);
            fragment.setArguments(bundle);
            searchLayout.setVisibility(View.GONE);
            findViewById(R.id.fragmentContainer).setVisibility(View.VISIBLE);
            
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
            
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
        }
    }
    
    void openSearchResults() {
        String searchString = searchEdit.getText().toString();
        if (searchString.length() > 0) {
            SearchResultsFragment fragment = new SearchResultsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("searchString", searchString);
            fragment.setArguments(bundle);
            searchLayout.setVisibility(View.GONE);
            findViewById(R.id.fragmentContainer).setVisibility(View.VISIBLE);
            
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    searchLayout.setVisibility(View.VISIBLE);
                    findViewById(R.id.fragmentContainer).setVisibility(View.GONE);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onBackPressed();
                        }
                    });
                }
            });
            
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();
        }
    }
}

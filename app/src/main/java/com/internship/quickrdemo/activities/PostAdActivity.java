package com.internship.quickrdemo.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.adapters.PostAdCategoriesRecyclerAdapter;
import com.internship.quickrdemo.fragments.FillAdDetailsFragment;
import com.internship.quickrdemo.interfaces.OnCategoryClickListener;
import com.internship.quickrdemo.models.CategoryList;

import java.util.ArrayList;

public class PostAdActivity extends AppCompatActivity {
    
    RecyclerView categoriesRecycler;
    PostAdCategoriesRecyclerAdapter adapter;
    
    FirebaseFirestore fStore;
    
    ArrayList<CategoryList> categoryLists = new ArrayList<>();
    
    Toolbar toolbar;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postad);
        
        fStore = FirebaseFirestore.getInstance();
        categoriesRecycler = findViewById(R.id.categoriesRecycler);
        
        toolbar = findViewById(R.id.toolbar);
        
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        
        adapter = new PostAdCategoriesRecyclerAdapter(new OnCategoryClickListener() {
            @Override
            public void onClick(CategoryList category) {
                FillAdDetailsFragment fragment = new FillAdDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("categoryId", category.getId());
                bundle.putString("categoryName", category.getName());
                fragment.setArguments(bundle);
                
                findViewById(R.id.fragmentContainer).setVisibility(View.VISIBLE);
                
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, fragment)
                        .commit();
                
                toolbar.setNavigationOnClickListener(null);
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog dialog = new AlertDialog.Builder(PostAdActivity.this)
                                .setTitle("Are you sure?")
                                .setMessage("Would you like to cancel posting?")
                                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        onBackPressed();
                                    }
                                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    
                                    }
                                }).create();
                        
                        dialog.show();
                    }
                });
            }
        });
        categoriesRecycler.setLayoutManager(new LinearLayoutManager(this));
        categoriesRecycler.setAdapter(adapter);
    
        fStore.collection("categories")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.e("DOCUMENT", document.getId());
                                CategoryList model = new CategoryList();
                                model.setId(document.getId());
                                model.setName(document.getString("name"));
                                model.setPicURL(document.getString("picURL"));
                                categoryLists.add(model);
                                adapter.setItems(model);
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.toString());
                e.printStackTrace();
            }
        });
    }
}

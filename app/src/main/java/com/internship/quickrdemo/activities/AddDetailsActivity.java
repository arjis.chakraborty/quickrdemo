package com.internship.quickrdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.internship.quickrdemo.R;
import com.internship.quickrdemo.constants.Constants;

import java.util.HashMap;
import java.util.Map;

public class AddDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    
    TextInputLayout name, email;
    Button continueBtn;
    
    String phNum;
    
    FirebaseFirestore fStore;
    CollectionReference collectionReference;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_details);
        
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        continueBtn = findViewById(R.id.continueBtn);
        
        phNum = getIntent().getStringExtra("phNum");
        
        fStore = FirebaseFirestore.getInstance();
        collectionReference = fStore.collection("users");
        
        continueBtn.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continueBtn:
                if (name.getEditText().getText().toString().isEmpty() || email.getEditText().getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your email and name", Toast.LENGTH_SHORT).show();
                } else {
                    //save all the data and go to main activity
                    saveData();
                }
                break;
        }
    }
    
    private void saveData() {
        Map<String, Object> user = new HashMap<>();
        user.put("name", name.getEditText().getText().toString());
        user.put("phNum", phNum);
        user.put("email", email.getEditText().getText().toString());
        user.put("loggedIn", "1");
        
        String user_id = System.currentTimeMillis() + "";
        
        collectionReference.document(user_id)
                .set(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Constants.SHARED_PREFERENCES.edit()
                                .putString(Constants.USER_ID, user_id)
                                .putString(Constants.USER_NAME, name.getEditText().getText().toString())
                                .putString(Constants.USER_PH, phNum)
                                .putString(Constants.USER_EMAIL, email.getEditText().getText().toString())
                                .putString(Constants.USER_LOGGED, "1")
                                .apply();
    
                        Intent intent = new Intent(AddDetailsActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AddDetailsActivity.this, "Error logging in", Toast.LENGTH_SHORT).show();
            }
        });
        
    }
}

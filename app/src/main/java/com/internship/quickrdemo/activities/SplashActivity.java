package com.internship.quickrdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.internship.quickrdemo.R;
import com.internship.quickrdemo.constants.Constants;

public class SplashActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        
        Constants.SHARED_PREFERENCES = getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        
        new Handler()
                .postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, "").isEmpty()) {
                            startLoginActivity();
                        } else {
                            startMainActivity();
                        }
                    }
                }, 1500);
    }
    
    void startLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    
    void startMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}

package com.internship.quickrdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.internship.quickrdemo.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextInputLayout phNumEdit;
    Button continueBtn;
    
    FirebaseFirestore fStore;
    CollectionReference collectionReference;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        fStore = FirebaseFirestore.getInstance();
        collectionReference = fStore.collection("users");
        
        phNumEdit = findViewById(R.id.phNumEdit);
        continueBtn = findViewById(R.id.continueBtn);
        
        continueBtn.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continueBtn:
                if (phNumEdit.getEditText().getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter a phone number", Toast.LENGTH_SHORT).show();
                } else {
                    //start verification activity
                    collectionReference
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {
                                        for (QueryDocumentSnapshot document : task.getResult()) {
                                            String phNum = (String) document.getData().get("phNum");
                                            if (phNum != null && phNum.equals("+91" + phNumEdit.getEditText().getText().toString())) {
                                                Log.e("DOCUMENT_ID", document.getId());
                                                Intent i = new Intent(LoginActivity.this, VerificationActivity.class);
                                                i.putExtra("phNum", phNumEdit.getEditText().getText().toString());
                                                i.putExtra("isExisting", true);
                                                startActivity(i);
                                                finish();
                                                break;
                                            }
                                        }
                                        Intent i = new Intent(LoginActivity.this, VerificationActivity.class);
                                        i.putExtra("phNum", phNumEdit.getEditText().getText().toString());
                                        startActivity(i);
                                        finish();
                                    } else {
                                        Log.e("ERROR", task.getException().toString());
                                    }
                                }
                            });
                }
                break;
        }
    }
    
    
}
